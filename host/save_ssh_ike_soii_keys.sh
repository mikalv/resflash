#!/bin/sh

# Save SSH, IKE, and SOII keys to /cfg
# Copyright Brian Conway <bconway@rcesoftware.com>, see LICENSE for details

set -o errexit
set -o nounset
if set -o|fgrep -q pipefail; then
  set -o pipefail
fi

echo 'Saving SSH, IKE, and SOII keys'

mount -s /cfg
trap 'sync; umount /cfg; exit 1' ERR INT

mkdir -p /cfg/etc
# tar -C doesn't play nicely with glob(3)
cwd=$(pwd)
cd /etc
tar cf - ssh/ssh_host_*key*|tar xvpf - -C /cfg/etc
tar cf - {isakmpd,iked}/local.pub|tar xvpf - -C /cfg/etc
tar cf - {isakmpd,iked}/private/local.key|tar xvpf - -C /cfg/etc
tar cf - soii.key|tar xvpf - -C /cfg/etc
cd ${cwd}

sync
umount /cfg
